import $ from "jquery";

const $header = $(".js-header");
const scrollStateClass = "_scroll";

function handleScroll() {
  const offset = 10;
  const isOffset = window.scrollY > offset;

  if (isOffset) {
    $header.addClass(scrollStateClass);
  } else {
    $header.removeClass(scrollStateClass);
  }
}

$(document).on("scroll", handleScroll);
