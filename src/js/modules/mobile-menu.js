import $ from "jquery";

// Mobile menu button
const menuStateClass = "_menu-open";
const $mobileMenu = $(".js-mobile-menu");
const animationDuration = 300;
const initMenuClass = "_menu-init";

let mobileMenuState = false;
const menuInitState = false;

// !! - Need to fix animation

function handleMenuBtn() {
  if (!menuInitState) {
    $("body").addClass(initMenuClass);
  }

  if (mobileMenuState) {
    mobileMenuState = false;
    $mobileMenu.slideDown(animationDuration);
  } else {
    mobileMenuState = true;
    $mobileMenu.slideUp(animationDuration);
  }

  $("body").toggleClass(menuStateClass);
}

const $mobileMenuBtn = $(".js-menu-btn");
$mobileMenuBtn.on("click", handleMenuBtn);
